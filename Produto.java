import java.util.ArrayList;
import java.util.Arrays;

public class Produto {
	private int countNegativo;
	private int vet[];
	private int tamanho;
	
	public Produto(int vet[]) {
		this.countNegativo = 0;
		this.vet = vet;
		this.tamanho = 0;
	}

	public void removeNegativo() 
	{
		for(int i = 0; i<tamanho;i++) 
		{
			if(vet[i]<0 && vet[i+1]>=0) 
			{
				for(int k = i; 	k<tamanho-1 ;k++) 
				{
					vet[k] = vet[k+1];
				}
				tamanho--;
				break;
			}
		}
	}
	
	public void removeZero() 
	{
		for(int i = 0;i<tamanho;i++) 
		{
			if(vet[i]==0) 
			{
					for(int k = i; 	k<tamanho-1 ;k++) 
					{
						vet[k] = vet[k+1];
					}
					tamanho--;
					break;
			}
		}
	}
	
	public void produtoMax() 
	{
		tamanho = vet.length;
		float p = 1;
		Arrays.sort(vet);
		for(int i = 0; i<tamanho;i++) 
		{
			if(vet[i]<0) 
			{
				countNegativo++;
			}
		}
		if(countNegativo%2!=0) 
		{
			removeNegativo();
		}
		removeZero();
		for(int i = 0;i<tamanho; i++) 
		{
			System.out.print(vet[i]+", ");
			p = p*vet[i];
		}
		System.out.println();
		System.out.print("Produto maximo � :"+p);
	}
	

}
